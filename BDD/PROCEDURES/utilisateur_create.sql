/*
 * Procedure qui créée un nouvel utilisateur.
 * Parametres:
                l = login de l'utilisateur.
*/

delimiter //

CREATE PROCEDURE utilisateur_create (IN l VARCHAR(63), IN pwd VARCHAR(255),
                                     IN psd VARCHAR(30), IN e VARCHAR(255),
                                     IN d INT(11))
  BEGIN
    INSERT INTO Utilisateur(login, password, pseudo, email, droit)
    VALUES(l, pwd, psd, e, d);
  END//

delimiter ;