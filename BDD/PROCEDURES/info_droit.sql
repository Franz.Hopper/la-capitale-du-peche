/*
 * Procedure qui renvoi les informations d'un droit.
 * Parametres: s La signification du droit.
*/

delimiter //

CREATE PROCEDURE info_droit (IN s ENUM("Visiteur", "Membre",
                                       "Modérateur", "Administrateur"))
  BEGIN
   SELECT id, hierarchie FROM Droit WHERE signification = s;
  END//

delimiter ;