/*
 * Procedure qui renvoi la hierarchie et la signification d'un droit.
 * Parametres: id_droit = L'id du droit dont on cherche les informations.
*/

delimiter //

CREATE PROCEDURE info_droit_id (IN id_droit INT(11))
  BEGIN
   SELECT hierarchie, signification FROM Droit WHERE id = id_droit;
  END//

delimiter ;