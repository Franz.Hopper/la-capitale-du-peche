/*
 * Procedure qui liste l'id, l'intitule, la moyenne, le pseudo et l'id 
 * du créateur de tous les commentaires d'un topic.
*/

delimiter //

CREATE PROCEDURE get_commentaires_topic (IN t INT(11))
  BEGIN
    SELECT c.id, contenu, note, date, pseudo FROM Commentaire c
    JOIN Utilisateur u ON createur = u.id
    WHERE topic = t ORDER BY date;
  END//

delimiter ;