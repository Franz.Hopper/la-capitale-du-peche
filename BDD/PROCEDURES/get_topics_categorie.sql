/*
 * Procedure qui liste l'id, l'intitule, la moyenne, le pseudo et l'id 
 * du créateur de tous les topics d'une catégorie.
*/

delimiter //

CREATE PROCEDURE get_topics_categorie (IN c INT(11))
  BEGIN
    SELECT t.id id, intitule, moyenne, pseudo, u.id user FROM Topic t
    JOIN Utilisateur u ON createur = u.id
    WHERE categorie = c ORDER BY derniere_modif DESC;
  END//

delimiter ;