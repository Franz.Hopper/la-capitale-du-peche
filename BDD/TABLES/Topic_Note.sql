CREATE TABLE IF NOT EXISTS Topic_Note
(
    id INT AUTO_INCREMENT,
    note FLOAT DEFAULT 0,
    juge INT NOT NULL,
    topic INT NOT NULL,

    PRIMARY KEY(id),
    UNIQUE(juge, topic),
    FOREIGN KEY(juge) REFERENCES Utilisateur(id),
    FOREIGN KEY(topic) REFERENCES Topic(id)
);