CREATE TABLE IF NOT EXISTS Commentaire
(
    id INT AUTO_INCREMENT,
    contenu TEXT NOT NULL,
    note TINYINT DEFAULT 0,
    date DATETIME NOT NULL,
    createur INT NOT NULL,
    topic INT NOT NULL,

    PRIMARY KEY(id),
    FOREIGN KEY(createur) REFERENCES Utilisateur(id),
    FOREIGN KEY(topic) REFERENCES Topic(id)
);