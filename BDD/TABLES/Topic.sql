CREATE TABLE IF NOT EXISTS Topic
(
    id INT AUTO_INCREMENT,
    intitule VARCHAR(255) NOT NULL,
    contenu TEXT,
    moyenne FLOAT DEFAULT -1,
    derniere_modif DATETIME NOT NULL,
    createur INT NOT NULL,
    categorie INT NOT NULL,

    PRIMARY KEY(id),
    FOREIGN KEY(createur) REFERENCES Utilisateur(id),
    FOREIGN KEY(categorie) REFERENCES Categorie(id),
    UNIQUE(intitule, categorie)
);