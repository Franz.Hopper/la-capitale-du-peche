/*
 * Fichier d'initialisation de la table Topic
 * 
 * Non nécessaire au bon fonctionnement du site.
 * Ces insertions permettent simplement d'avoir un site non vide au départ...
*/


INSERT INTO Topic(intitule, contenu, derniere_modif, createur, categorie)
  VALUES
  (
    "Selon Wikipédia",
    "L'orgueil (superbia en latin) est une opinion très avantageuse, le plus
     souvent exagérée, qu'on a de sa valeur personnelle aux dépens de la
     considération due à autrui, à la différence de la fiertén 1 qui n'a nul
     besoin de se mesurer à l'autre ni de le rabaisser.",
    NOW(),
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    (SELECT id FROM Categorie WHERE intitule = "L'orgueil")
  );

INSERT INTO Topic(intitule, contenu, derniere_modif, createur, categorie)
  VALUES
  (
    "Selon Wikipédia",
    "L’avarice est un état d’esprit qui consiste à ne pas vouloir se séparer de
     ses biens et richesses.",
    NOW(),
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    (SELECT id FROM Categorie WHERE intitule = "L'avarice")
  );


INSERT INTO Topic(intitule, contenu, derniere_modif, createur, categorie)
  VALUES
  (
    "Selon Wikipédia",
    "La luxure est un terme d'origine religieuse, qui désigne un penchant
     considéré comme immodéré pour la pratique des plaisirs sexuels.
     Elle peut aussi renvoyer à une sexualité sans vocation procréative,
     désordonnée ou incontrôlée.",
    NOW(),
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    (SELECT id FROM Categorie WHERE intitule = "La luxure")
  );


INSERT INTO Topic(intitule, contenu, derniere_modif, createur, categorie)
  VALUES
  (
    "Selon Wikipédia",
    "L’envie (du latin : invidia) est l'objet de plusieurs définitions et sens.
     Elle peut être synonyme de désir, ou désigner un ressentiment et créer
     un désir face au bonheur d'autrui ou à ses avantages.
     Bertrand Russell explique que l'envie est la plus importante des causes
     de malheur moral3. Elle ne doit pas être confondue avec la jalousie,
     qui consiste à ne pas vouloir partager ou perdre son bien.",
    NOW(),
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    (SELECT id FROM Categorie WHERE intitule = "L'envie")
  );

INSERT INTO Topic(intitule, contenu, derniere_modif, createur, categorie)
  VALUES
  (
    "Selon Wikipédia",
    "La gourmandise est un désir d'aliments jugés particulièrement agréables.
     Au XIXe siècle, des Français établissent une distinction entre gourmandise
     et goinfrerie, considérant la première comme une qualité, la seconde
     comme un défaut.",
    NOW(),
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    (SELECT id FROM Categorie WHERE intitule = "La gourmandise")
  );

INSERT INTO Topic(intitule, contenu, derniere_modif, createur, categorie)
  VALUES
  (
    "Selon Wikipédia",
    "En psychologie, la colère est considérée comme une émotion secondaire
     liée à une blessure physique ou psychique, un manque, une frustration.
     Celle-ci se caractérise généralement par une réaction vive entraînant
     généralement des manifestations physiques ou psychologiques de la part
     de la personne concernée, celle-ci pouvant cependant être contenue,
     voire dissimulée.
     Selon certains philosophes grecs, notamment Aristote, la colère peut
     faire souffrir celui qui l'exprime et peut être ainsi considérée
     comme une passion.",
    NOW(),
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    (SELECT id FROM Categorie WHERE intitule = "La colère")
  );

INSERT INTO Topic(intitule, contenu, derniere_modif, createur, categorie)
  VALUES
  (
    "Selon Wikipédia",
    "L’acédie est un concept moral, religieux et psychologique qui a pris des
     sens différents selon les cultures dans lesquelles il est utilisé.
     Elle signifie à l'origine un manque de soin. La notion fut ensuite
     christianisée par les pères du désert pour désigner un manque de soin
     pour sa vie spirituelle1. La conséquence de cette négligence est un mal de
     l’âme qui s’exprime par l’ennui, ainsi que le dégoût pour la prière,
     la pénitence et la lecture spirituelle. Quand l'acédie devient un état de
     l’âme qui entraîne une torpeur spirituelle et un repli sur soi, elle est
     une maladie spirituelle.",
    NOW(),
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    (SELECT id FROM Categorie WHERE intitule = "La paresse")
  );