/*
 * Fichier d'initialisation de la table Droit
*/

INSERT INTO Droit(hierarchie, signification) VALUES(0, "Visiteur");
INSERT INTO Droit(hierarchie, signification) VALUES(1, "Membre");
INSERT INTO Droit(hierarchie, signification) VALUES(2, "Modérateur");
INSERT INTO Droit(hierarchie, signification) VALUES(3, "Administrateur");