/*
 * Fichier d'initialisation de la table Categorie
 * 
 * Non nécessaire au bon fonctionnement du site.
 * Ces insertions permettent simplement d'avoir un site non vide au départ...
*/


INSERT INTO Categorie(intitule, createur, dateCreation)
  VALUES
  (
    "L'orgueil",
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    CURDATE()
  );

INSERT INTO Categorie(intitule, createur, dateCreation)
  VALUES
  (
    "L'avarice",
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    CURDATE()
  );

INSERT INTO Categorie(intitule, createur, dateCreation)
  VALUES
  (
    "La luxure",
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    CURDATE()
  );

INSERT INTO Categorie(intitule, createur, dateCreation)
VALUES
(
  "L'envie",
  (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
  CURDATE()
);

INSERT INTO Categorie(intitule, createur, dateCreation)
  VALUES
  (
    "La gourmandise",
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    CURDATE()
  );

INSERT INTO Categorie(intitule, createur, dateCreation)
  VALUES
  (
    "La colère",
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    CURDATE()
  );

INSERT INTO Categorie(intitule, createur, dateCreation)
  VALUES
  (
    "La paresse",
    (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
    CURDATE()
  );