<?php

require_once "model_base.php";

class Commentaire_Note extends Model_Base
{
  protected $_id;
  protected $_note;
  protected $_juge;
  protected $_commentaire;

  public function __construct()
  {
    $ctp = func_num_args(); // nb arguments passés
    $args= func_get_args(); // tableau avec liste arguments
    
    switch($ctp)
    {
      case 3: // note, commentaire, juge
      {
        $this->set_juge($args[2]);
      }
      case 2: // note, commentaire
      {
        $this->set_commentaire($args[1]);
      }
      case 1: // note
      {
        $this->set_note($args[0]);
        break;
      }
      default:
      {
        throw new Exception("Nombre d'arguments du contructeur incorrect.");
      }
    }
  }

  private function set_id($id)
  {
    $this->_id = $id;
  }

  public function get_id()
  {
    return $this->_id;
  }


  public function set_note($n)
  {
    if($n >= 0)
      $this->_note = 1;
    else
      $this->_note = -1;
  }


  public function get_note()
  {
    return $this->_note;
  }


  public function set_juge($g)
  {
    $this->_juge = $g;
  }

  public function get_juge()
  {
    return $this->_juge;
  }


  public function set_commentaire($c)
  {
    $this->_commentaire = $c;
  }

  public function get_commentaire()
  {
    return $this->_commentaire;
  }


  public function noter()
  {
    $statement = self::$_db->prepare('CALL noter_commentaire(:n, :j, :c)');
    $statement->bindValue(':n', $this->get_note(), PDO::PARAM_INT);
    $statement->bindValue(':j', $this->get_juge(), PDO::PARAM_INT);
    $statement->bindValue(':c', $this->get_commentaire(), PDO::PARAM_INT);
    $statement->execute();
    
    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      return $result['note'];
    }

    else return 0;
  }

  public function retirer_note()
  {
    $statement = self::$_db->prepare('CALL retirer_note_commentaire(:j, :c)');
    $statement->bindValue(':j', $this->get_juge(), PDO::PARAM_INT);
    $statement->bindValue(':c', $this->get_commentaire(), PDO::PARAM_INT);
    $statement->execute();
    
    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      return $result['note'];
    }

    else return 0;
  }
}