<?php

require_once "model_base.php";

class Topic_Note extends Model_Base
{
  protected $_id;
  protected $_note;
  protected $_juge;
  protected $_topic;

  public function __construct()
  {
    $ctp = func_num_args(); // nb arguments passés
    $args= func_get_args(); // tableau avec liste arguments
    
    switch($ctp)
    {
      case 3: // note, topic, juge
      {
        $this->set_juge($args[2]);
      }
      case 2: // note, topic
      {
        $this->set_topic($args[1]);
      }
      case 1: // note
      {
        $this->set_note($args[0]);
        break;
      }
      default:
      {
        throw new Exception("Nombre d'arguments du contructeur incorrect.");
      }
    }
  }

  private function set_id($id)
  {
    $this->_id = $id;
  }

  public function get_id()
  {
    return $this->_id;
  }


  public function set_note($n)
  {
    if($n < 0)
      $this->_note = 0;
    else if($n > 10)
      $this->_note = 10;
    else
      $this->_note = $n;
  }


  public function get_note()
  {
    return $this->_note;
  }


  public function set_juge($g)
  {
    $this->_juge = $g;
  }

  public function get_juge()
  {
    return $this->_juge;
  }


  public function set_topic($t)
  {
    $this->_topic = $t;
  }

  public function get_topic()
  {
    return $this->_topic;
  }


  public function noter()
  {
    $statement = self::$_db->prepare('CALL noter_topic(:n, :j, :t)');
    $statement->bindValue(':n', $this->get_note(), PDO::PARAM_INT);
    $statement->bindValue(':j', $this->get_juge(), PDO::PARAM_INT);
    $statement->bindValue(':t', $this->get_topic(), PDO::PARAM_INT);
    $statement->execute();
    
    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      return $result['moyenne'];
    }

    else return 0;
  }

  public function retirer_note()
  {
    $statement = self::$_db->prepare('CALL retirer_note_topic(:j, :t)');
    $statement->bindValue(':j', $this->get_juge(), PDO::PARAM_INT);
    $statement->bindValue(':t', $this->get_topic(), PDO::PARAM_INT);
    $statement->execute();
    
    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      return $result['note'];
    }

    else return 0;
  }
}