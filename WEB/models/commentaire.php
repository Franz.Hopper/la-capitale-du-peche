<?php

require_once "model_base.php";

class Commentaire extends Model_Base implements JsonSerializable
{
  protected $_id;
  protected $_contenu;
  protected $_note;
  protected $_date;
  protected $_createur;
  protected $_pseudo_createur;
  protected $_topic;

  public function __construct()
  {
    $ctp = func_num_args(); // nb arguments passés
    $args= func_get_args(); // tableau avec liste arguments
    
    switch($ctp)
    {
      case 3: // contenu, createur, topic
      {
        $this->set_topic($args[2]);
      }
      case 2: // contenu, createur
      {
        $this->set_createur($args[1]);
      }
      case 1: // contenu
      {
        $this->set_contenu($args[0]);
        break;
      }
      default:
      {
        throw new Exception("Nombre d'arguments du contructeur incorrect.");
      }
    }
  }

  public function set_id($id)
  {
    $this->_id = $id;
  }

  public function get_id()
  {
    return $this->_id;
  }


  public function set_contenu($c)
  {
    $this->_contenu = $c;
  }

  public function get_contenu()
  {
    return $this->_contenu;
  }


  public function set_note($n)
  {
    $this->_note = $n;
  }

  public function get_note()
  {
    return $this->_note;
  }


  public function set_date($d)
  {
    $this->_date = $d;
  }

  public function get_date()
  {
    return $this->_date;
  }


  public function set_createur($c)
  {
    $this->_createur = $c;
  }

  public function get_createur()
  {
    return $this->_createur;
  }


  public function set_pseudo_createur($p)
  {
    $this->_pseudo_createur = $p;
  }

  public function get_pseudo_createur()
  {
    return $this->_pseudo_createur;
  }


  public function set_topic($t)
  {
    $this->_topic = $t;
  }

  public function get_topic()
  {
    return $this->_topic;
  }


  // Redéfinition de la fonction json_encode
  public function jsonSerialize()
  {
    return 
    [
      'id'   => $this->get_id(),
      'contenu' => $this->get_contenu(),
      'note' => $this->get_note(),
      'date' => $this->get_date(),
      'createur' => $this->get_createur(),
      'pseudo_createur' => $this->get_pseudo_createur(),
      'topic' => $this->get_topic()
    ];
  }


  public function commenter()
  {
    self::$_db->setAttribute(PDO::ATTR_EMULATE_PREPARES,TRUE);
    $statement = self::$_db->prepare('CALL commenter(:c, :crea, :t)');
    $statement->bindValue(':c', $this->get_contenu(), PDO::PARAM_STR);
    $statement->bindValue(':crea', $this->get_createur(), PDO::PARAM_STR);
    $statement->bindValue(':t', $this->get_topic(), PDO::PARAM_STR);
    if(!($statement->execute()))
    {
      return 1; // Erreur lors de l'insertion
    }
    
    $this->set_note(0);
    $this->set_id($statement->fetch(PDO::FETCH_ASSOC)['id']);
    return 0; // OK
  }
}