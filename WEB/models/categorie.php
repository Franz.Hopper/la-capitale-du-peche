<?php

require_once "model_base.php";
require_once "topic.php";

class Categorie extends Model_Base
{
  protected $_id;
  protected $_intitule;
  protected $_createur;
  protected $_dateCreation;
  protected $_listeTopics;


  public function __construct()
  {
    $ctp = func_num_args(); // nb arguments passés
    $args= func_get_args(); // tableau avec liste arguments
    
    switch($ctp)
    {
      case 3: // intitule, createur, date de creation
      {
        $this->set_dateCreation($args[2]);
      }
      case 2: // intitule, createur
      {
        $this->set_createur($args[1]);
      }
      case 1: // Juste l'intitule
      {
        $this->set_intitule($args[0]);
        break;
      }
      default:
      {
        throw new Exception("Nombre d'arguments du contructeur incorrect.");
      }
    }
  }


  private function set_id($id)
  {
    $this->_id = $id;
  }

  public function get_id()
  {
    return $this->_id;
  }


  public function set_intitule($i)
  {
    $this->_intitule = $i;
  }

  public function get_intitule()
  {
    return $this->_intitule;
  }


  public function set_createur($c)
  {
    $this->_createur = $c;
  }

  public function get_createur()
  {
    return $this->_createur;
  }


  public function set_dateCreation($d)
  {
    $this->_dateCreation = $d;
  }

  public function get_dateCreation()
  {
    return $this->_dateCreation;
  }


  public function set_listeTopics($t)
  {
    $this->_listeTopics = $t;
  }

  public function get_listeTopics()
  {
    return $this->_listeTopics;
  }

  

  public static function getCategories()
  {
    self::$_db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    $statement = self::$_db->query('CALL liste_categories()');

    // Initialisation d'un tableau vide : s'il n'existe aucune catégorie
    $table = array();
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    foreach($result as $tabVal)
    {
      $categorie = new Categorie($tabVal['intitule']);
      $categorie->set_id($tabVal['id']);

      // Récupération de la liste des topics d'une catégorie
      $req = self::$_db->prepare('CALL get_topics_categorie(:id);');
      $req->bindValue(':id', $categorie->get_id(), PDO::PARAM_INT);
      $req->execute();
        

      $tabTopics = array();
      while($res = $req->fetch(PDO::FETCH_ASSOC))
      {
        $topic = new Topic($res['intitule']);
        $topic->set_id($res['id']);
        $topic->set_moyenne($res['moyenne']);
        $topic->set_pseudo_createur($res['pseudo']);
        $tabTopics[] = serialize($topic);
      };
      $categorie->set_listeTopics($tabTopics);
      $req->closeCursor();


      $table[] = serialize($categorie);
    }
    return $table;
  }
}