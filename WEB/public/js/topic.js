// Fonction d'appel au serveur
var HttpClient = function(paquet)
{
  this.get = function(aUrl, likePost, aCallback)
  {
    var anHttpRequest = new XMLHttpRequest();

    anHttpRequest.onreadystatechange = function()
    {
      // Si la requete a été complètement traitée
      if(anHttpRequest.readyState == 4)
      {
        // aCallback contient la reponse du serveur
        aCallback(JSON.parse(anHttpRequest.responseText));
      }
    }
    // Parametres de l'envoi
    anHttpRequest.open("POST", aUrl, true);
    if(likePost)
    {
      anHttpRequest.setRequestHeader("Content-type",
                                     "application/x-www-form-urlencoded");
    }
    anHttpRequest.send(paquet);
  }
}


// Fonction d'envoi du formulaire (poster un commentaire)
var form = document.getElementById('commenter');
form.onsubmit = function()
{ 
  var sendingButton = document.getElementById("sending_button");
  var buttonSpan = document.getElementById("button_span");
  var sendingButtonText = document.getElementById("button_text");
  var errorDisplay =  document.getElementById("msg_sending");

  // Chargement apparent
  buttonSpan.classList.add("spinner-border", "spinner-border-sm");
  sendingButtonText.innerHTML = "Chargement...";
  sendingButton.disabled = true;

  var formData = new FormData(form);
  var client = new HttpClient(formData);
  client.get(root+"index.php/categories|commenter|"+idTopic, false,
             function(response)
  {
    if(response.success)
    {
      var div = document.getElementById("comment_area");
      var com = JSON.parse(response.content);

      var div1 = document.createElement("div");
      div1.classList.add("d-flex", "align-items-center", "w-100");

      var div2 = document.createElement("div");
      div2.classList.add("d-flex", "flex-column", "align-items-center", "ml-2");
      div2.classList.add("mr-2" ,"align-middle");
      div1.appendChild(div2);

      var button1 = document.createElement('button');
      button1.id = "plus_" + com['id'];
      button1.classList.add("arrowMark");
      button1.setAttribute('onclick', "plussoyer("+com['id']+")");
      button1.innerHTML = "&#9650;";
      div2.appendChild(button1);

      var span = document.createElement('span');
      span.id = com['id'];
      span.innerHTML = com['note'];
      div2.appendChild(span);

      var button2 = document.createElement('button');
      button2.id = "moins_" + com['id'];
      button2.classList.add("arrowMark");
      button2.setAttribute('onclick', "moinssoyer("+com['id']+")");
      button2.innerHTML = "&#9660;";
      div2.appendChild(button2);

      var div3 = document.createElement('div');
      div3.classList.add("bg-light", "border", "border-dark", "m-2", "p-3");
      div3.classList.add("w-100");
      div3.id = "com_"+com['id'];
      div1.appendChild(div3);

      var p1 = document.createElement('p');
      p1.innerHTML = com['pseudo_createur'] + " - Posté à l'instant";
      div3.appendChild(p1);
      var p2 = document.createElement('p');
      p2.innerHTML = com['contenu'];
      div3.appendChild(p2);

      div.appendChild(div1);
    }
    else
    {
      errorDisplay.innerHTML = response.content;
      errorDisplay.style.display = "flex";
      errorDisplay.focus();
    }

    // On enlève l'apparence du chargement
    buttonSpan.classList.remove("spinner-border", "spinner-border-sm");
    sendingButtonText.innerHTML = "Commenter";        
    sendingButton.disabled = false;
  });
  
  return false; // Pour éviter l'envoi du réel formulaire
}

// Noter positivement un commentaire
function plussoyer(id)
{
  var button = document.getElementById("plus_"+id);
  var moins = document.getElementById("moins_"+id);

  if(moins.classList.contains('estActif'))
  {
    moins.classList.remove("estActif");
    button.classList.add("estActif");

    var client = new HttpClient("note=1");
    client.get(root+"index.php/notation|noter_com|"+id, true,
               callBack);
  }
  else if(button.classList.contains('estActif'))
  {
    button.classList.remove("estActif");

    var client = new HttpClient("note=0");
    client.get(root+"index.php/notation|noter_com|"+id, true,
               callBack);
  }
  else
  {
    button.classList.add("estActif");

    var client = new HttpClient("note=1");
    client.get(root+"index.php/notation|noter_com|"+id, true,
               callBack);
  }
}


// Noter négativement un commentaire
function moinssoyer(id)
{
  var button = document.getElementById("moins_"+id);
  var plus = document.getElementById("plus_"+id);

  if(plus.classList.contains('estActif'))
  {
    plus.classList.remove("estActif");
    button.classList.add("estActif");

    var client = new HttpClient("note=-1");
    client.get(root+"index.php/notation|noter_com|"+id, true,
               callBack);
  }
  else if(button.classList.contains('estActif'))
  {
    button.classList.remove("estActif");

    var client = new HttpClient("note=0");
    client.get(root+"index.php/notation|noter_com|"+id, true,
               callBack);
  }
  else
  {
    button.classList.add("estActif");

    var client = new HttpClient("note=-1");
    client.get(root+"index.php/notation|noter_com|"+id, true,
               callBack);
  }
}


// Fonction callBack appelé après la notation d'un commentaire
function callBack(response)
{
  if(response.success)
  {
    changeNote(response.id, response.content);
  }
  else
  {
    var errorDisplay = document.getElementById("msg-alert-top");
    errorDisplay.innerHTML = response.content;
    errorDisplay.style.display = "flex";
    errorDisplay.focus();
  }
}

/*
 * Fonction qui change l'affichage de la note d'un commentaire ainsi que 
 * la couleur de fond d'un commentaire selon sa pertinence.
*/
 function changeNote(id, val)
{
  var note = document.getElementById(id);
  var com = document.getElementById("com_"+id);

  note.innerText = val;  

  if(val < 0)
  {
    com.classList.remove("bg-light");
    com.classList.add("bg-sec", "text-white");
  }
  else
  {
    com.classList.add("bg-light");
    com.classList.remove("bg-sec", "text-white");
  }
}



document.getElementById("Notation").addEventListener('mouseover', mouse_over);
document.getElementById("Notation").addEventListener('mouseout', mouse_out);


// Fonction qui change l'affichage de la note du topic (étoiles)
function changer_note(elem)
{
  var obj = elem;
  var obj_left = 0;
  var maxW = elem.offsetWidth;
  var xpos;
  while (obj.offsetParent)
  {
    obj_left += obj.offsetLeft;
    obj = obj.offsetParent;
  }
  if(event)
  {
    //FireFox
    xpos = event.pageX;
  }
  else
  {
    //IE
    xpos = window.event.x + document.body.scrollLeft - 2;
  }
  xpos -= obj_left;

  note = 10*(xpos/maxW);
  document.getElementById("full-stars").style.width = 10*note+"%";
  document.getElementById("valeurNote").innerHTML= Math.round(note*10)/10+"/10";
}

// Changement d'apparence au hoover
function mouse_out(elem, text)
{
  document.getElementById("valeurNote").innerText =  moyenneTopic+"/10";
  document.getElementById("typeNote").innerText =  "Moyenne : ";
  document.getElementById("full-stars").style.color = "#1c5f9b";
  document.getElementById("full-stars").style.width = 10*moyenneTopic+"%";
}

// Retour à l'état initial
function mouse_over()
{
  document.getElementById("typeNote").innerText =  "Votre note : ";
  document.getElementById("full-stars").style.color = "brown";
  document.getElementById("full-stars").style.width = "0%";
}


// Fonction d'envoi de la note du topic au serveur
function noter(id)
{
  // Affichage envoi
  var errDisplay = document.getElementById("msg-alert-topic-note");
  errDisplay.classList.remove("alert-danger","alert-success");
  while(errDisplay.firstChild)
  {
    errDisplay.removeChild(errDisplay.firstChild);
  }
  var div = document.createElement("div");
  div.classList.add("d-flex", "justify-content-center", "w-100");
  errDisplay.appendChild(div);
  var div2 = document.createElement("div");
  div2.classList.add("spinner-border");
  div.appendChild(div2);
  errDisplay.style.display = "flex";

  var note = parseFloat(document.getElementById("full-stars").style.width);
  note = Math.round(note)/10;
  
  var client = new HttpClient("note="+note);
  client.get(root+"index.php/notation|noter_topic|"+id, true, function(response)
  {
    while(errDisplay.firstChild)
    {
      errDisplay.removeChild(errDisplay.firstChild);
    }

    if(response.success)
    {
      moyenneTopic = response.content;
      errDisplay.classList.add("alert-success");
      errDisplay.innerText = "Votre note a bien été ajoutée !"
    }
    else
    {
      errDisplay.classList.add("alert-danger");
      errDisplay.innerHTML = response.content;
      errDisplay.focus();
    }
  });
}

