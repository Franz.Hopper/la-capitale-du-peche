<?php
$found = false;

session_start();
if(empty($_SESSION['root']))
{
  $args = explode('/', $_SERVER['REQUEST_URI']);
  $_SESSION['root'] = "";
  $argsSize = count($args)-1;
  if($args[$argsSize-1] === "index.php")
  {
    $argsSize--;
  }
  for($i=0; $i<$argsSize; $i++)
  {
    $_SESSION['root'] .= $args[$i]."/";
  }
}



// Suppression du dernier '/'
$_SERVER['PATH_INFO'] = ltrim($_SERVER['PATH_INFO'], '/');

$args = explode('|', $_SERVER['PATH_INFO']);
$argsSize = count($args);


// Taille décrémentée si dernière partie vide
if(empty($args[$argsSize-1]))
{
  $argsSize -= 1;
}


/*
 * Controlleur par défaut : accueil
 * Methode par défaut : view
*/
switch($argsSize)
{
  case 0:
  {
    $controller = "accueil";
    $method = "view";
    $params = array();
    break;
  }
  case 1:
  {
    $controller = $args[0];
    $method = "view";
    $params = array();
    break;
  }
  default:
  {
    $controller = $args[0];
    $method = $args[1];
    $params = array();
    for($i=2; $i<$argsSize; $i++)
    {
      $params[] = $args[$i];
    }
  }
}

$controller_file = 'controllers/' .$controller.'.php';
if(is_file($controller_file))
{
  require_once $controller_file;
  $controller_name = 'Controller_' .ucfirst($controller);
  if(class_exists($controller_name))
  {
    $c = new $controller_name;
    if(method_exists($c, $method))
    {
      call_user_func_array(array($c, $method), $params);
      $found = true;
    }
  }
}

if(!$found)
{
  http_response_code(404);
  $_SESSION['corps'] = 'views/errors/404.php';
  require_once 'controllers/accueil.php';
  $c = new Controller_Accueil;
  $c->view();
  unset($_SESSION['corps']);
}