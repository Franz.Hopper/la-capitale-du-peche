<?php if($_SERVER['REQUEST_METHOD'] == 'GET')
{?>
  <!doctype html>
    <html lang="fr">
    <head>
      <title>La Capitale du Péché</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1,
                                     shrink-to-fit=no">
      <meta name="description" content="Forum sur les péchés capitaux"/>
      <meta name="keyword"
            content="Forum, péchés, discussion, bla bla, prog web"/>
      <meta name="author" content="Timothée ZERBIB"/>
      <link rel="icon" type="image/png" 
            href="<?php echo $_SESSION['root'];?>public/images/favicon.png">
      <link rel="shortcut icon" type="image/x-icon"
            href="<?php echo $_SESSION['root'];?>public/images/favicon.ico">
      <link rel="stylesheet"
            href=
    "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity=
    "sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
              integrity=
    "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
              crossorigin="anonymous"></script>
      <script src=
    "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
              integrity=
    "sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
              crossorigin="anonymous"></script>
      <script src=
    "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
              integrity=
    "sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
              crossorigin="anonymous"></script>
      <script src="<?php echo $_SESSION['root'];?>public/js/submenu.js">
      </script>
      <link rel="stylesheet" 
            href="<?php echo $_SESSION['root'];?>public/css/base.css">
      <link rel="stylesheet" 
            href="<?php echo $_SESSION['root'];?>public/css/notation.css">
      <?php echo $_SESSION['include']; ?>

    </head>

    <body>
      <div class="bloc_page">
        <?php include_once 'viewHeader.php'; ?>
        <div id="background">
          <?php include_once 'viewNavbar.php'; ?>
          <div>
            <?php include_once 'viewNav.php'; ?>

            <div class="d-flex flex-column align-items-center text-center p-3">
              <div class="main-container border border-dark m-3 p-4 w-75">
                <?php
                if(isset($_SESSION['message']))
                {
                  $msg = $_SESSION['message'];
                  unset($_SESSION['message']);
                }
                else
                {
                  $class = "alert-area";
                }
                echo '<div class="alert alert-danger mr-sm-2 my-lg-0 '.$class.'"
                          role="alert" id="msg-alert-top">';
                  echo $msg;
                echo '</div>';
                ?>
                <div id="corps">
                  <?php include_once $_SESSION['corps']; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <footer id="Zfooter" class="p-5">
      <div class="back-overlay">
      </div>
      <a class="btn btn-info btn-lg m-5"
         href="<?php echo $_SESSION['root'];?>index.php/accueil|contact">
         Contact</a>
      <a class="btn btn-info btn-lg m-5"
         href="<?php echo $_SESSION['root'];?>index.php/accueil|aPropos">
         A propos</a>
      <div class="gitlab">
        <a href="https://gitlab.unistra.fr/tzerbib/la-capitale-du-peche">
          <img src="<?php echo $_SESSION['root'];?>public/images/gitlab-logo.png">
        </a>
      </div>
    </footer>
    </body>
  </html>
<?php
}?>