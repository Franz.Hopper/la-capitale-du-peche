<h1 class="m-3">Liste des catégories</h1>

<div class="mt-5">
  <?php
  if(isset($_SESSION['Categories']) && count($_SESSION['Categories']) > 0)
  {
    foreach($_SESSION['Categories'] as $value)
    {
      $Categorie = unserialize($value);
      $id = $Categorie->get_id();

      echo '<div class="m-4 mt-5 mb-5">';
        echo "<h2 class='mb-4'>".$Categorie->get_intitule()."</h2>";

        echo '<table class="table table-striped table-dark table-hover">';
          echo '<thead class="bg-success">';
            echo '<tr>';
              echo '<th scope="col">Moyenne</th>';
              echo '<th scope="col">Intitulé</th>';
              echo '<th scope="col" colspan="2">Créateur</th>';
            echo '</tr>';
          echo '</thead>';

          echo '<tbody>';
          if(count($Categorie->get_listeTopics()) > 0)
          {
            foreach($Categorie->get_listeTopics() as $listeTopics)
            {
              $Topic = unserialize($listeTopics);
              $note = $Topic->get_moyenne()*10;
              echo '<tr>';
                echo '<td class="pl-0 pr-0">';
                  echo '<div class="semi-stars">';
                    echo '<div class="full-stars" style="width: '.$note.'%;">';
                    echo '</div>';
                  echo '</div>';
                echo '</td>';
                echo '<th scope="row" class="position-relative">
                  '.$Topic->get_intitule();
                  echo '<a class="stretched-link" 
                           href=
          "'.$_SESSION['root'].'index.php/categories|view|'.$Topic->get_id().'">
                        </a>';
                echo '</th>';
                echo '<td>'.$Topic->get_pseudo_createur().'</td>';
                echo '</tr>';
            }
          }
          else
          {
            echo '<p>Aucun topic pour cette catégorie</p>';
          }
          echo '</tbody>';
        echo '</table>';
      echo '</div>';
    }
  }
  else
  {
    echo "<p>Il n'existe aucune catégorie pour le moment</p>";
  }
  ?>
</div>