<h1 class="m-3">Bienvenue !</h1>

<div>
  <p>
    Garçon ou fille, grand ou petit, on a tous nos petites envies.
  </p>
  <p>  
    Parfois incontenables, souvent inavouables, je vous propose ici de
    les partager pour mieux s'en défaire.
    Allez-y, jouez le jeu, après tout, ce n'est pas souvent que l'on peut
    se permettre d'exposer ses vices sans risques d'être jugé ! ;)
  </p>
  <p class="mt-5 mb-5">Je terminerai simplement sur ces quelques citations...</p>

  <blockquote class="blockquote">
    <p class="mb-0">"Plaire à soi est orgueil, aux autres vanité."</p>
    <footer class="blockquote-footer">Paul Valéry</footer>
  </blockquote>

  <blockquote class="blockquote">
    <p class="mb-0">"Lorgueil a cela de bon qu'il préserve de l'envie."</p>
    <footer class="blockquote-footer">Victor Hugo</footer>
  </blockquote>

  <blockquote class="blockquote">
    <p class="mb-0">“La conscience ? Elle n'empêche jamais de commettre
       un péché. Elle empêche seulement d'en jouir en paix !”</p>
    <footer class="blockquote-footer">Theodore Dreiser</footer>
  </blockquote>
</div>