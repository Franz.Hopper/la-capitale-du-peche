<h1 class="m-3">Contact</h1>

<div>
  <p>
    Une remarque, une suggestion ou simplement envie de discuter ?<br/>
    Tu peux me contacter à l'aide de
    <a href="mailto:timothee.zerbib@etu.unistra.fr">ce lien</a> ;)
  </p>
  <p>
    N'oublie pas d'aller voir mon 
    <a href="https://gitlab.unistra.fr/tzerbib">gitlab</a> !
  </p>
</div>