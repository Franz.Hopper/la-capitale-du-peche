<div class=
  "navbar navbar-expand-lg sticky-top navbar-dark bg-dark justify-content-end">
  <div class="d-flex flex-row col order-first">
    <a href=<?php echo $_SESSION['root'];?>>
      <img src="<?php echo $_SESSION['root'];?>public/images/favicon.png"
          width="50" height="50" class="d-inline-block align-top" alt="">
      </a>
      <a href=<?php echo $_SESSION['root'];?> class="nav-link navbar-brand">
        Accueil
      </a>
  </div>
      <?php
      if($User->get_connecte())
      { ?>
        <div class="d-flex justify-content-end">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <p class="nav-link mb-0"><?php echo $User->get_pseudo();?></p>
            </li>
            <li class="nav-item active">
              <a class="btn btn-danger my-2 my-sm-0"
                href=<?php echo $_SESSION['root']."index.php/accueil|signout";?>>
                Se deconnecter
              </a>
            </li>
          </ul>
        </div>
      <?php }
      else
      {
        if(isset($_SESSION['msg_co']))
        {
          echo '<div class="alert alert-danger mr-sm-2 my-lg-0" role="alert">';
            echo $_SESSION['msg_co'];
            unset($_SESSION['msg_co']);
          echo '</div>';
        }
        ?>
        <form method="post" class="form-inline m-auto"
              action=
              <?php echo $_SESSION['root']."index.php/accueil|authenticate";?>>
          <label for="login" class="sr-only">pseudo</label>
            <input type="text" id="login" name="login"
                   class="form-control mr-sm-2" placeholder="Login" required>
          <label for="password" class="sr-only">password</label>
            <input type="password" id="password" name="password"
                   class="form-control mr-sm-2" placeholder="Password" required>
          <input type="submit" class="btn btn-success mr-sm-2"
                 value="Se connecter">
        </form>
        <a href="<?php echo $_SESSION['root'];?>index.php/register|view">
          <input type="submit" class="btn btn-primary" id="inscription"
                 value="S'inscrire"/>
        </a>
        <?php
      } ?>
</div>