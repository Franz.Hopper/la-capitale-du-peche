<nav class="d-flex float-left shadow-lg rounded">
  <div class="row">
    <div class="col-3">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
           aria-orientation="vertical">
        <a class="nav-link active" id="v-pills-home-tab"
           href="<?php echo $_SESSION['root'];?>" role="tab"
           aria-controls="v-pills-home" aria-selected="true">
           Accueil
        </a>
        
        <div class="dopdown">
          <div class="btn-group dropright">
            <a type="button" class="btn nav-link"
              href=
                "<?php echo $_SESSION['root'];?>index.php/categories|viewAll">
              Categories
            </a>

            <button type="button" data-toggle="dropdown" aria-haspopup="true"
                    class="btn dropdown-toggle dropdown-toggle-split"
                    aria-expanded="false">
                    <span class="sr-only"></span>
            </button>

            <ul class="dropdown-menu m-0 p-0">

              <?php
              // Création d'un menu déroulant des catégories
              if(isset($_SESSION['Categories']) 
              && count($_SESSION['Categories']) > 0)
              {
                foreach($_SESSION['Categories'] as $value)
                {
                  $Categorie = unserialize($value);

                  echo '<li class="dropdown-submenu">';
                    echo '<button type="button" class="btn btn-info
                           dropdown-toggle submenu w-100">
                           '.$Categorie->get_intitule().'
                          </button>';
                         
                    echo '<ul class="dropdown-menu p-0">';
                    if(count($Categorie->get_listeTopics()) > 0)
                    {
                      foreach($Categorie->get_listeTopics() as $listeTopics)
                      {
                        $Topic = unserialize($listeTopics);
                        echo "<li><a class='dropdown-item'
                                     href=
        '".$_SESSION['root']."index.php/categories|view|".$Topic->get_id()."'>";
                        echo $Topic->get_intitule();
                        echo "</a></li>";
                      }
                    }
                    else
                    {
                      echo '<p class="mb-0 p-2">
                              Aucun topic pour cette catégorie</p>';
                    }
                    echo '</ul>';
                  echo '</li>';
                }
              }
              else
              {
                echo "<li><p class='mb-0 p-2'>
                        Il n'existe aucune catégorie pour le moment
                      </p></li>";
              } ?>
            </div>
          </ul>
        </div>
        
        
        <a class="nav-link" id="v-pills-profile-tab"
           href="#" role="tab"
           aria-controls="v-pills-profile" aria-selected="false">Profile</a>

        <?php
        if($User->hasPermissions("Administrateur"))
        {
          echo '<a class="nav-link" id="v-pills-profile-tab"
          href="'.$_SESSION['root'].'index.php/utilisateurs|viewAll"
          role="tab"
          aria-controls="v-pills-profile" aria-selected="false">';
          echo 'Membres</a>';
        } ?>
      </div>
  </div>
</nav>