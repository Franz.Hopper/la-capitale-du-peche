<?php
class Controller_Register
{
  public function view()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {    
      include_once 'views/viewRegister.php';  
    }
  }

  public function register()
  {
    if($_SERVER['REQUEST_METHOD'] != 'POST')
    {
      header('Location: '.$_SESSION['root']);
      exit;
    }

    if(!(isset($_POST['login']) && isset($_POST['pseudo'])
      && isset($_POST['inputPassword']) && isset($_POST['confirmPassword'])
      && isset($_POST['email'])))
    {
      $_SESSION['message'] = "Toutes les infos n'ont pas été transmises";      
      header('Location: '.$_SESSION['root'].'index.php/register|view');
      exit;
    }

    $login = htmlentities($_POST['login']);
    $pseudo = htmlentities($_POST['pseudo']);
    $password = htmlentities($_POST['inputPassword']);
    $passwordConfirm = htmlentities($_POST['confirmPassword']);
    $email = htmlentities($_POST['email']);
    
    // Vérification de l'égalité des deux champs de mot de passe
    if($password !== $passwordConfirm)
    {
      $_SESSION['message'] = "Les deux mots de passe doivent être identiques !";      
      header('Location: '.$_SESSION['root'].'index.php/register|view');
      exit;
    }

    // Vérification de la validité du champ email
    if(!preg_match("#^[A-Za-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $email))
    {
      $_SESSION['message'] = "L'email doit être valide !";      
      header('Location: '.$_SESSION['root'].'index.php/register|view');
      exit;
    }

    try
    {
      require_once 'models/bdd.php';
      require_once 'models/model_base.php';
      Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
    }
    catch(Exception $e)
    {
      $_SESSION['message'] = "Erreur lors de la connexion à la BDD : ";
      $_SESSION['message'] .= $e->getMessage();
      header('Location: '.$_SESSION['root'].'index.php/register|view');
      exit;
    }
    
    require_once "models/users.php";
    $User = new User($login, $password, $pseudo, $email);
    
    switch($User->create())
    {
      case 0: // SUCCESS
      {
        $_SESSION['user'] = serialize($User);
        header('Location: '.$_SESSION['root']);
        exit;
      }
      case 1: // Erreur lors de l'insertion dans la table
      {
        $_SESSION['message'] = "Erreur lors de l'insertion dans la table";
        break;
      }
      case 2: // Duplicate login
      {
        $_SESSION['message'] = "Ce login existe déjà !";
        break;
      }
      case 3: // Duplicate pseudo
      {
        $_SESSION['message'] = "Ce pseudo existe déjà !";
        break;
      }
      case 4: // Duplicate email
      {
        $_SESSION['message'] = "Cet email est déjà utilisé pour un autre
                                compte !";
        break;
      }
    }

    header('Location: '.$_SESSION['root'].'index.php/register|view');
    exit;
  }
}