<?php
class Controller_Notation
{
  public function noter_com()
  {
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      $response = array();
      require_once 'models/users.php';
      
      // Cas sans informations formulaire (note)
      if(!isset($_POST['note']))
      {
        $response['success'] = false;
        $response['content'] = 'Information minimales requises !';
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }
      
      // Cas sans parametre de la méthode (id commentaire)
      if(func_num_args() != 1)
      {
        $response['success'] = false;
        $response['content'] = "Informations manquantes !";
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }
      
      // Vérification des droits
      if(!isset($_SESSION['user']) 
      || !unserialize($_SESSION['user'])->get_connecte())
      {
        $response['success'] = false;
        $response['content']="Seuls les membres peuvent noter les commentaires";
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }
      
      $args = func_get_args(); // tableau avec liste arguments

      try
      {
        require_once 'models/bdd.php';
        require_once 'models/model_base.php';
        require_once 'models/commentaire_note.php';
        Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
      }
      catch(Exception $e)
      {
        $response['success'] = false;
        $response['content'] = "Erreur lors de la connexion à la BDD : ";
        $response['content'] .= $e->getMessage();
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }

      $n = htmlentities($_POST['note']);
      $c = new Commentaire_Note
      (
        (int)$n,
        $args[0],
        unserialize($_SESSION['user'])->get_id()
      );

      if($n == 0)
      {
        $response['content'] = $c->retirer_note();
      }
      else
      {
        $response['content'] = $c->noter();
      }

      $response['id'] = $args[0];
      $response['success'] = true;
      $JSON_response = json_encode($response);
      echo $JSON_response;
    }
    else
    {
      $_SESSION['corps'] = 'views/errors/404.php';
      require_once 'controllers/accueil.php';
      $c = new Controller_Accueil;
      $c->view();
      unset($_SESSION['corps']);
      exit;
    }
  }


  public function noter_topic()
  {
    if($_SERVER['REQUEST_METHOD'] != 'POST')
    {
      $_SESSION['corps'] = 'views/errors/404.php';
      require_once 'controllers/accueil.php';
      $c = new Controller_Accueil;
      $c->view();
      unset($_SESSION['corps']);
      exit;
    }

    $response = array();
    require_once 'models/users.php';
    
    // Cas sans informations formulaire (note)
    if(!isset($_POST['note']))
    {
      $response['success'] = false;
      $response['content'] = 'Information minimales requises !';
      $JSON_response = json_encode($response);
      echo $JSON_response;
      exit;
    }
    
    // Cas sans parametre de la méthode (id topic)
    if(func_num_args() != 1)
    {
      $response['success'] = false;
      $response['content'] = "Informations manquantes !";
      $JSON_response = json_encode($response);
      echo $JSON_response;
      exit;
    }
    
    // Vérification des droits
    if(!isset($_SESSION['user']) 
    || !unserialize($_SESSION['user'])->get_connecte())
    {
      $response['success'] = false;
      $response['content']="Seuls les membres peuvent noter les topics";
      $JSON_response = json_encode($response);
      echo $JSON_response;
      exit;
    }
    
    $args = func_get_args(); // tableau avec liste arguments

    try
    {
      require_once 'models/bdd.php';
      require_once 'models/model_base.php';
      require_once 'models/topic_note.php';
      Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
    }
    catch(Exception $e)
    {
      $response['success'] = false;
      $response['content'] = "Erreur lors de la connexion à la BDD : ";
      $response['content'] .= $e->getMessage();
      $JSON_response = json_encode($response);
      echo $JSON_response;
      exit;
    }

    $n = htmlentities($_POST['note']);
    $c = new Topic_Note
    (
      (float)$n,
      $args[0],
      unserialize($_SESSION['user'])->get_id()
    );

    $response['content'] = $c->noter();
    $response['success'] = true;
    $JSON_response = json_encode($response);
    echo $JSON_response;
  }
}