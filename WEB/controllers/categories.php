<?php
class Controller_Categories
{
  public function viewAll()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      // Changement du corps de la page
      $_SESSION['corps'] = 'views/corps/viewCorpsCategories.php';
      require_once 'controllers/accueil.php';
      $c = new Controller_Accueil;
      $c->view();
      unset($_SESSION['corps']);
    }
  }

  public function view()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      $ctp = func_num_args(); // nb arguments passés
      $args= func_get_args(); // tableau avec liste arguments
      
      switch($ctp)
      {
        case 0:
        {
          header('Location: '.$_SESSION['root'].'index.php/categories|viewAll');
          exit;
        }
        case 1:
        {
          $tid = $args[0];
          $page = 1;
          break;
        }
        case 2:
        {
          $tid = $args[0];
          $page = $args[1];
          break;
        }
        default:
        {
          // Changement du corps de la page (erreur 404)
          $_SESSION['corps'] = 'views/errors/404.php';
          require_once 'controllers/accueil.php';
          $c = new Controller_Accueil;
          $c->view();
          unset($_SESSION['corps']);
          exit;
        }
      }

      try
      {
        require_once 'models/bdd.php';
        require_once 'models/model_base.php';
        Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
      }
      catch(Exception $e)
      {
        $_SESSION['message'] = "Erreur lors de la connexion à la BDD : ";
        $_SESSION['message'] .= $e->getMessage();
        header('Location: '.$_SESSION['root'].'index.php/categories|viewAll');
        exit;
      }

      $args = func_get_args();
      
      require_once 'models/topic.php';
      $t = new Topic();
      $t->set_id($tid);

      // Si le topic n'existe pas
      if($t->exists() != 0)
      {
        // Changement du corps de la page (erreur 404)
        $_SESSION['corps'] = 'views/errors/404.php';
        require_once 'controllers/accueil.php';
        $c = new Controller_Accueil;
        $c->view();
        unset($_SESSION['corps']);
        exit;
      }
      
      // Changement du corps de la page avec le topic
      $_SESSION['Topic'] = serialize($t);
      $_SESSION['corps'] = 'views/corps/viewCorpsTopic.php';
      $_SESSION['include'] .= "<link rel='stylesheet' href='"
                              .$_SESSION['root']."public/css/topic.css'>";
     
      require_once 'controllers/accueil.php';
      $c = new Controller_Accueil;
      $c->view();
      unset($_SESSION['corps']);
      unset($_SESSION['Topic']);
      unset($_SESSION['include']);
    }
  }


  public function commenter()
  {
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      $response = array();
      require_once 'models/users.php';
      
      // Cas sans informations formulaire (commentaire)
      if(!isset($_POST['newCom']))
      {
        $response['success'] = false;
        $response['content'] = 'Information minimales requises !';
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }
      
      // Cas sans parametre de la méthode (id topic)
      if(func_num_args() != 1)
      {
        $response['success'] = false;
        $response['content'] = "Informations manquantes !";
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }
      
      // Vérification des droits
      if(!isset($_SESSION['user']) 
      || !unserialize($_SESSION['user'])->get_connecte())
      {
        $response['success'] = false;
        $response['content'] = "Seuls les membres peuvent commenter les topics";
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }
      
      $args = func_get_args(); // tableau avec liste arguments
      
      try
      {
        require_once 'models/bdd.php';
        require_once 'models/model_base.php';
        require_once 'models/commentaire.php';
        Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
      }
      catch(Exception $e)
      {
        $response['success'] = false;
        $response['content'] = "Erreur lors de la connexion à la BDD : ";
        $response['content'] .= $e->getMessage();
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }

      $contenu = htmlentities($_POST['newCom']);
      $c = new Commentaire($contenu,
                           unserialize($_SESSION['user'])->get_id(), $args[0]);
      $c->set_pseudo_createur(unserialize($_SESSION['user'])->get_pseudo());
      if($c->commenter() == 1)
      {
        $response['success'] = false;
        $response['content'] = "Erreur lors de l'insertion dans la table : ";
        $response['content'] .= $e->getMessage();
        $JSON_response = json_encode($response);
        echo $JSON_response;
        exit;
      }

      $response['success'] = true;
      $response['content'] = json_encode($c);
      $JSON_response = json_encode($response);
      echo $JSON_response;
    }
    else
    {
      $_SESSION['corps'] = 'views/errors/404.php';
      require_once 'controllers/accueil.php';
      $c = new Controller_Accueil;
      $c->view();
      unset($_SESSION['corps']);
      exit;
    }
  }


  static public function liste_categories()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      try
      {
        require_once 'models/bdd.php';
        require_once 'models/model_base.php';
        Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
      }
      catch(Exception $e)
      {
        $_SESSION['message'] = "Erreur lors de la connexion à la BDD : ";
        $_SESSION['message'] .= $e->getMessage();
        header('Location: '.$_SESSION['root']);
        exit;
      }

      require_once "models/categorie.php";
      return Categorie::getCategories();
    }
  }
}